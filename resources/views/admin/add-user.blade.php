@extends('../layout/admin', ['title' => 'Add User'])

@section('content')
<form method="POST" action="{{ route('add-user') }}">
    @csrf
    <div class="form-group">
        <label>Name</label>
        <input type="text" name="name" class="form-control">
        @error('name')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>

    <div class="form-group">
        <label>Email</label>
        <input type="text" name="email" class="form-control">
        @error('email')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>

    <div class="form-group">
        <label>Password</label>
        <input type="password" name="password" class="form-control">
        @error('password')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>

    <button type="submit" class="btn btn-primary">Add</button>
</form>
@endsection