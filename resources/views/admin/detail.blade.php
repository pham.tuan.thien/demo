@extends('../layout/admin', ['title' => 'View'])

@section('content')
<table class="table table-bordered">
    <tr>
        <td>Name</td>
        <td>{{ $user->name }}</td>
    </tr>
    <tr>
        <td>Email</td>
        <td>{{ $user->email }}</td>
    </tr>
    <tr>
        <td>Status</td>
        <td>{{ $user->status == 1 ? 'Active' : 'Inactive' }}</td>
    </tr>
</table>
<a href="{{ route('update-user', ['id' => $user->id]) }}">
    <button class="btn btn-primary">Update</button>
</a>
<form method="POST" action="{{ route('delete-user', [$user->id]) }}" onsubmit="return ConfirmDelete( this )">
    @method('DELETE')
    @csrf
    <button type="submit" class="btn btn-danger">Delete</button>
</form>
@endsection