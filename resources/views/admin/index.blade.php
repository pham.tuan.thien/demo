@extends('../layout/admin', ['title' => 'Home'])

@section('content')
<div class="card">
    <div class="card-header">
        User list
        <div class="float-right">
            <a href="{{ route('add-user') }}">
                <button class="btn btn-success">
                    Add user
                </button>
            </a>
        </div>
    </div>
    <div class=card-body>
        <div class="">
            <form method="GET" action=" {{ route('dashboard') }} ">
                <div class="row">
                    <div class="col-5">
                        <div class="form-group">
                            <label>Name</label>
                            <input name="name" class="form-control" type="text" value="{{ $params->name }}">
                        </div>
                    </div>

                    <div class="col-5">
                        <div class="form-group">
                            <label>Email</label>
                            <input name="email" class="form-control" type="text" value="{{ $params->email }}">
                        </div>
                    </div>

                    <div class="col-2">
                        <div class="form-group">
                            <label>Status</label>
                            <select class="custom-select" name="status">
                                <option selected value="1">Active</option>
                                <option value="0">Inactive</option>
                            </select>
                        </div>
                    </div>
                </div>

                <button type="submit" class="btn btn-primary">Search</button>
            </form>
        </div>
        <hr>
        <table class="table table-bordered">
            <tr class="text-center">
                <th width="5%">Id</th>
                <th>Name</th>
                <th>Email</th>
                <th>Status</th>
            </tr>
            @foreach ($users as $user)
            <tr>
                <td class="text-center">{{ $user->id }}</td>
                <td><a href="{{ url('/admin/view-user', ['id' => $user->id]) }}">{{ $user->name }}</td>
                <td>{{ $user->email }}</td>
                <td class="text-center">{{ $user->status == 1 ? 'Active' : 'Inactive' }}</td>
            </tr>
            @endforeach
        </table>
    </div>
</div>
@endsection