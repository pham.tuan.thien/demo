@extends('../layout/user', ['title' => $news->title])

@section('content')
<div class="com-text">
    {{ $news->content }}
</div>
@endsection