@extends('../layout/user', ['title' => 'New post'])

@section('content')
<form method="POST" action=" {{ route('write') }}">
    @csrf
    <div class="form-group">
        <label>Title</label>
        <input type="text" class="form-control" name="title">
        @error('title')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group">
        <label>Content</label>
        <textarea class="form-control" rows="5" name="content"></textarea>
        @error('content')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>

    <button type="submit" class="btn btn-success"><div class="px-2">Create</div></button>
</form>
@endsection