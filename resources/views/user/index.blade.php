@extends('../layout/user', ['title' => 'Home'])

@section('content')
<div class="card">
    <div class="card-header">
        Posts
        <div class="float-right">
            <a href="{{ route('write') }}">
                <button class="btn btn-success">
                    New post
                </button>
            </a>
        </div>
    </div>
    <div class=card-body>
        <table class="table table-bordered">
            <tr class="text-center">
                <th width="5%">Id</th>
                <th>Title</th>
                <th>Content</th>
                <th width="15%">Status</th>
            </tr>
            @foreach ($news as $post)
            <tr>
                <td class="text-center">{{ $post->id }}</td>
                <td><a href="{{ url('/view', ['id' => $post->id]) }}">{{ $post->title }}</td>
                <td class="com-text">{{ $post->content }}</td>
                <td class="text-center">{{ $status[$post->status] }}</td>
            </tr>
            @endforeach
        </table>
    </div>
</div>
@endsection