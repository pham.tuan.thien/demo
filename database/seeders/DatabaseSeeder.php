<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admins')->insert([
            [
                'username' => 'superadmin',
                'password' => bcrypt('admin123'),
                'status' => 1,
            ]
        ]);

        DB::table('users')->insert([
            [
                'name' => 'Pham Tuan Thien',
                'email' => 'thienpt@sun-asterisk.com',
                'password' => bcrypt('admin123'),
                'status' => 1,
            ]
        ]);
    }
}
