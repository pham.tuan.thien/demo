<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\LoginController;
use App\Http\Controllers\Admin\HomeController;

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "admin" middleware group. Now create something great!
|
*/

Route::match(['get', 'post'], '/login', [LoginController::class, 'login'])->name('admin.login');

Route::middleware('auth:admin')->group(function () {
    Route::get('/home', [HomeController::class, 'index'])->name('dashboard');
    Route::get('/logout', [LoginController::class, 'logout']);

    Route::match(['get', 'post'], '/add-user', [HomeController::class, 'addUser'])->name('add-user');
    Route::match(['get', 'post'], '/update-user/{id}', [HomeController::class, 'updateUser'])->name('update-user');
    Route::get('/view-user/{id}', [HomeController::class, 'viewUser'])->name('view-user');
    Route::delete('/delete-user/{id}', [HomeController::class, 'deleteUser'])->name('delete-user');
});