<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

use App\Models\News;

class HomeController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $news = News::where(['user_id' => $user->id])->get();
        $status = [
            News::NEW_POST => 'New post',
            News::APPROVED => 'Approved',
            News::REJECTED => 'Rejected',
        ];

        return view('user.index', [
            'user' => $user,
            'news' => $news,
            'status' => $status,
        ]);
    }

    public function write(Request $request)
    {
        $user = Auth::user();

        if ($request->getMethod() == 'POST') {
            $validated = $request->validate([
                'title' => 'required|max:255',
                'content' => 'required',
            ]);

            $news = new News([
                'user_id' => $user->id,
                'title' => $request->title,
                'content' => $request->content,
                'status' => News::NEW_POST,
            ]);
            $news->save();
            return redirect()->route('view', ['id' => $news->id]);
        }

        return view('user.write', [
            'user' => $user
        ]);
    }

    public function view($id)
    {
        $user = Auth::user();

        $news = News::find($id);

        return view('user.view', [
            'user' => $user,
            'news' => $news,
        ]);
    }
}
