<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;

use App\Models\User;
use App\Models\News;

class HomeController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | HomeController
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    public function guard()
    {
        return Auth::guard('admin');
    }

    public function index(Request $request)
    {
        $admin = Auth::guard('admin')->user();
        $params = $request->all();
        $users = User::list($params);

        return view('admin.index', [
            'admin' => $admin,
            'users' => $users,
            'params' => $request,
        ]);
    }

    public function addUser(Request $request)
    {
        $admin = Auth::guard('admin')->user();

        if ($request->getMethod() == 'POST') {
            $validated = $request->validate([
                'name' => 'required|max:255',
                'email' => 'required|unique:users,email|email',
                'password' => 'required',
            ]);

            $info = $request->post();
            $info['password'] = Hash::make($request->password);
            $info['status'] = 1;
            $user = User::create($info);

            return redirect()->route('view-user', ['id' => $user->id]);
        }

        return view('admin.add-user', [
            'admin' => $admin,
        ]);
    }

    public function viewUser($id)
    {
        $admin = Auth::guard('admin')->user();
        $user = User::find($id);

        return view('admin.detail', [
            'admin' => $admin,
            'user' => $user,
        ]);
    }

    public function updateUser(Request $request, $id)
    {
        $admin = Auth::guard('admin')->user();
        $user = User::find($id);

        if ($request->getMethod() == 'POST') {
            $validated = $request->validate([
                'name' => 'required|max:255',
                'email' => 'required|email|unique:users,email,' . $user->id,
            ]);

            $user->name = $request->name;
            $user->email = $request->email;
            $user->save();
            return redirect()->route('view-user', ['id' => $user->id]);
        }

        return view('admin.update', [
            'admin' => $admin,
            'user' => $user,
        ]);
    }

    public function deleteUser($id)
    {
        $user = User::find($id);
        $user->delete();

        return redirect()->route('dashboard');
    }
}
