<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    CONST NEW_POST = 1;
    CONST APPROVED = 2;
    CONST REJECTED = 0;
    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'user_id',
        'title',
        'content',
        'status',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
