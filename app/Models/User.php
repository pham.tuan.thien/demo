<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    CONST ACTIVE = 1;
    CONST INACTIVE = 0;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'status',
    ];

    /**
     * The default value of attributes
     */
    protected $attributes = [
        'status' => self::ACTIVE,
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Get all user for gridview
     */
    public static function list($params)
    {
        $query = User::query()
            ->select('id', 'name', 'email', 'status');
        foreach ($params as $attribute => $value) {
            if (isset($value)) {
                $query->where($attribute , 'like', "%$value%");
            }
        }
        return $query->get();
    }

    public function news()
    {
        return $this->hasMany(News::class);
    }

    public function delete()
    {
        News::where(['user_id' => $this->id])->delete();
        parent::delete();
    }
}
